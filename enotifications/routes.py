from webapp2 import Route

from handlers import CustomerActivationNotificationHandler, CustomerActivationNotificationSender, \
    CustomerModificationNotificationHandler, CustomerModificationNotificationSender, DiscountCodeNotificationHandler, \
    DiscountCodeNotificationSender, ManagerCreationNotificationHandler, ManagerCreationNotificationSender, \
    CustomerOrderCreationNotificationHandler, CustomerOrderCreationNotificationSender

service_routes = map(lambda r: Route(methods=r[0], template=r[1], handler=r[2]), [
    (['POST'], '/notification/customer-activation/',                   CustomerActivationNotificationHandler),
    (['GET'],  '/notification/customer-activation/list/',              CustomerActivationNotificationHandler),
    (['PUT'],  '/notification/customer-activation/mark-as-completed/', CustomerActivationNotificationHandler),
    (['GET'],  '/notification/customer-activation/send/',              CustomerActivationNotificationSender),

    (['POST'], '/notification/customer-modification/',                   CustomerModificationNotificationHandler),
    (['GET'],  '/notification/customer-modification/list/',              CustomerModificationNotificationHandler),
    (['PUT'],  '/notification/customer-modification/mark-as-completed/', CustomerModificationNotificationHandler),
    (['GET'],  '/notification/customer-modification/send/',              CustomerModificationNotificationSender),

    (['POST'], '/notification/discount-code/',                   DiscountCodeNotificationHandler),
    (['GET'],  '/notification/discount-code/list/',              DiscountCodeNotificationHandler),
    (['PUT'],  '/notification/discount-code/mark-as-completed/', DiscountCodeNotificationHandler),
    (['GET'],  '/notification/discount-code/send/',              DiscountCodeNotificationSender),

    (['POST'], '/notification/manager/',                   ManagerCreationNotificationHandler),
    (['GET'],  '/notification/manager/list/',              ManagerCreationNotificationHandler),
    (['PUT'],  '/notification/manager/mark-as-completed/', ManagerCreationNotificationHandler),
    (['GET'],  '/notification/manager/send/',              ManagerCreationNotificationSender),

    (['POST'], '/notification/order-creation/',                   CustomerOrderCreationNotificationHandler),
    (['GET'],  '/notification/order-creation/list/',              CustomerOrderCreationNotificationHandler),
    (['PUT'],  '/notification/order-creation/mark-as-completed/', CustomerOrderCreationNotificationHandler),
    (['GET'],  '/notification/order-creation/send/',              CustomerOrderCreationNotificationSender),
])

import webapp2

from routes import service_routes

application = webapp2.WSGIApplication(service_routes, debug=True)

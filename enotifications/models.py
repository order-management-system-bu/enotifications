from google.appengine.ext import ndb
from utility_module import validation_utils
from utility_module.abstractmodel import AbstractModel
from validate_email import validate_email


class EmailNotification(ndb.Model):
    """
    Model class represents a single email notification.
    """
    target_email = ndb.StringProperty()
    notification_sent = ndb.BooleanProperty()
    sent_date = ndb.DateTimeProperty()


class InNotificationIds(AbstractModel):
    """
    Model class represents a list of email notification IDs.
    """
    mandatory_fields = ['notification_ids']

    def __init__(self, notification_ids):
        self.notification_ids = notification_ids

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        return faults

    @classmethod
    def _read_json(cls, json):
        return InNotificationIds(notification_ids=json.get('notification_ids', None))


class DiscountCodeNotification(EmailNotification):
    """
    Model class represents a single discount code email
    notification.
    """
    target_email = ndb.StringProperty(repeated=True)
    discount_title = ndb.StringProperty()
    discount_code = ndb.StringProperty()


class InDiscountCodeNotification(AbstractModel):
    """
    Model class represents a single inbound discount code
    email notification.
    """
    mandatory_fields = ['target_email', 'discount_title', 'discount_code']

    def __init__(self, target_email, discount_title, discount_code):
        self.target_email = target_email
        self.discount_title = discount_title
        self.discount_code = discount_code

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        if self.target_email and len(filter(lambda e: not validate_email(e), self.target_email)) > 0:
            faults.append("One or more of the provided email addresses where not valid.")
        return faults

    @classmethod
    def _read_json(cls, json):
        return InDiscountCodeNotification(
            target_email=json.get('target_email', None),
            discount_title=json.get('discount_title', None),
            discount_code=json.get('discount_code', None)
        )


class CustomerModificationNotification(EmailNotification):
    """
    Model class represents a single customer modification email
    notification.
    """
    full_name = ndb.StringProperty()
    date_updated = ndb.StringProperty()
    field_names = ndb.StringProperty(repeated=True)


class InCustomerModificationNotification(AbstractModel):
    """
    Model class represents a single inbound customer modification
    email notification.
    """
    mandatory_fields = ['target_email', 'full_name', 'date_updated', 'field_names']

    def __init__(self, target_email, full_name, date_updated, field_names):
        self.target_email = target_email
        self.full_name = full_name
        self.date_updated = date_updated
        self.field_names = field_names

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        if self.target_email and not validate_email(self.target_email):
            faults.append('The provided email address was invalid.')
        return faults

    @classmethod
    def _read_json(cls, json):
        return InCustomerModificationNotification(
            target_email=json.get('target_email', None),
            full_name=json.get('full_name', None),
            date_updated=json.get('date_updated', None),
            field_names=json.get('field_names', None)
        )


class CustomerActivationNotification(EmailNotification):
    """
    Model class represents a single customer activation email
    notification.
    """
    message = ndb.TextProperty()


class InCustomerActivationNotification(AbstractModel):
    """
    Model class represents a single inbound customer activation
    email notification.
    """
    mandatory_fields = ['target_email', 'message']

    def __init__(self, target_email, message):
        self.target_email = target_email
        self.message = message

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        faults += validation_utils.verify_max_length(self, ['message'], 500)
        if self.target_email and not validate_email(self.target_email):
            faults.append('The provided email address was invalid.')
        return faults

    @classmethod
    def _read_json(cls, json):
        return InCustomerActivationNotification(
            target_email=json.get('target_email', None),
            message=json.get('message', None)
        )


class ManagerCreationNotification(EmailNotification):
    """
    Model class represents a single manager creation email
    notification.
    """
    password = ndb.TextProperty()


class InManagerCreationNotification(AbstractModel):
    """
    Model class represents a single inbound manager creation
    email notification.
    """
    mandatory_fields = ['target_email', 'password']

    def __init__(self, target_email, password):
        self.target_email = target_email
        self.password = password

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        if self.target_email and not validate_email(self.target_email):
            faults.append('The provided email address was invalid.')
        return faults

    @classmethod
    def _read_json(cls, json):
        return InManagerCreationNotification(
            target_email=json.get('target_email', None),
            password=json.get('password', None)
        )


class InCustomerOrderCreationNotification(AbstractModel):
    """
    Model class represents a single inbound customer order creation
    email notification.
    """
    mandatory_fields = [
        'target_email', 'order_number', 'products',
        'contact_number', 'street', 'city', 'county', 'postcode'
    ]

    def __init__(
            self, target_email, order_number, products, contact_number,
            street, city, county, postcode
    ):
        self.target_email = target_email
        self.order_number = order_number
        self.products = products
        self.contact_number = contact_number
        self.street = street
        self.city = city
        self.county = county
        self.postcode = postcode

    def validate(self):
        faults = validation_utils.verify_mandatory_fields(self)
        if self.target_email and not validate_email(self.target_email):
            faults.append('The provided email address was invalid.')
        return faults

    @classmethod
    def _read_json(cls, json):
        return InCustomerOrderCreationNotification(
            target_email=json.get('target_email', None),
            order_number=json.get('order_number', None),
            products=json.get('products', None),
            contact_number=json.get('contact_number', None),
            street=json.get('street', None),
            city=json.get('city', None),
            county=json.get('county', None),
            postcode=json.get('postcode', None)
        )


class CustomerOrderCreationNotification(EmailNotification):
    class Product(ndb.Model):
        title = ndb.StringProperty()
        price = ndb.StringProperty()

    """
    Model class represents a single customer order creation email
    notification.
    """
    order_number = ndb.StringProperty()
    products = ndb.StructuredProperty(Product, repeated=True)
    contact_number = ndb.StringProperty()
    street = ndb.StringProperty()
    city = ndb.StringProperty()
    county = ndb.StringProperty()
    postcode = ndb.StringProperty()

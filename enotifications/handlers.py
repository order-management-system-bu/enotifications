# encoding=utf8
import sys

reload(sys)
sys.setdefaultencoding('utf8')  # Super hacky fix to prevent encoding errors.

import httplib
import json as json_lib
import logging
import os
from datetime import datetime

from google.appengine.ext import ndb
from jinja2 import Template
from sendgrid import SendGridAPIClient, Email
from sendgrid.helpers.mail import Mail, Content
from utility_module.abstractresthandler import AbstractRestHandler

from models import InNotificationIds, InDiscountCodeNotification, DiscountCodeNotification, \
    InCustomerModificationNotification, CustomerModificationNotification, InCustomerActivationNotification, \
    CustomerActivationNotification, ManagerCreationNotification, InManagerCreationNotification, \
    CustomerOrderCreationNotification, InCustomerOrderCreationNotification
from sendgrid_configuration import SENDGRID_CONFIGURATION

# BASE_ENDPOINT = 'http://127.0.0.1:8081/'
BASE_ENDPOINT = 'https://e-notifications-230119.appspot.com/'


class DiscountCodeNotificationHandler(AbstractRestHandler):
    """
    Handler class is responsible for exposing discount code
    endpoints to external entities.
    """

    def create_discount_code_notification(self):
        """
        API call will attempt to create a new discount code notification
        request. Once created, the notification will be picked up and
        sent by the appropriate CRON job.
        """
        in_notification = InDiscountCodeNotification.convert_to_model(self.request.body)
        validation_faults = in_notification.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        DiscountCodeNotification(
            target_email=in_notification.target_email,
            discount_title=in_notification.discount_title,
            discount_code=in_notification.discount_code,
            notification_sent=False,
            sent_date=None
        ).put()
        return self.http_response(httplib.CREATED)

    def list_discount_code_notification(self):
        """
        API call will attempt to retrieve all pending discount
        code notifications.
        """
        pending_notifications = DiscountCodeNotification.query(
            DiscountCodeNotification.notification_sent == False)
        response_json = map(lambda n: {
            'id': n.key.pairs()[0][1],
            'target_email': n.target_email,
            'discount_title': n.discount_title,
            'discount_code': n.discount_code
        }, pending_notifications)
        return self.http_response(httplib.NOT_FOUND) if len(response_json) < 1 else \
            self.http_response(httplib.OK, response_json)

    def mark_as_completed(self):
        """
        API call will attempt to mark a set of pending discount
        code notifications as complete.
        """
        in_notification_ids = InNotificationIds.convert_to_model(self.request.body)
        validation_faults = in_notification_ids.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)

        notification = DiscountCodeNotification.query(
            DiscountCodeNotification.key == ndb.Key('DiscountCodeNotification',
                                                    int(in_notification_ids.notification_ids))).fetch()[0]
        notification.notification_sent = True
        notification.sent_date = datetime.now()
        notification.put()
        return self.http_response(httplib.OK)

    post = create_discount_code_notification
    get = list_discount_code_notification
    put = mark_as_completed


class DiscountCodeNotificationSender(AbstractRestHandler):
    """
    Handler class is responsible for exposing discount code notification
    sender endpoints.
    """

    def send_discount_code_notifications(self):
        """
        API call is responsible for constructing and sending all
        currently pending discount code notifications.
        """
        internal_discount_code_endpoint = BASE_ENDPOINT + 'notification/discount-code'
        response = self.http_request(endpoint=internal_discount_code_endpoint + "/list/", method='GET')
        if response.status_code == httplib.OK:
            notifications = json_lib.loads(response.content)['success']
            template_directory = os.path.dirname(
                os.path.realpath(__file__)) + '/templates/discount_code_template.html'
            with open(template_directory) as template_file:
                template_content = template_file.read()
                api_client = SendGridAPIClient(apikey=SENDGRID_CONFIGURATION['api_key'])
                for email in notifications[0]['target_email']:
                    template = Template(template_content).render(
                        discount_code=notifications[0]['discount_code'],
                        discount_title=notifications[0]['discount_title']
                    )
                    to_email = Email(email)
                    from_email = Email(SENDGRID_CONFIGURATION['from_email'])
                    subject = 'Thank You for Being Our Customer'
                    content = Content('text/html', template)
                    mail = Mail(from_email, subject, to_email, content)
                    api_client.client.mail.send.post(request_body=mail.get())
                self.http_request(
                    endpoint=internal_discount_code_endpoint + '/mark-as-completed/', method='PUT',
                    payload={'notification_ids': notifications[0]['id']})

    get = send_discount_code_notifications


class CustomerModificationNotificationHandler(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer modification
    notification request endpoints to external entities.
    """

    def create_customer_modification_notification(self):
        """
        API call will attempt to create a new customer modification notification
        request. Once created, the notification will be picked up and sent by
        the appropriate CRON job.
        """
        in_notification = InCustomerModificationNotification.convert_to_model(self.request.body)
        validation_faults = in_notification.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        CustomerModificationNotification(
            target_email=in_notification.target_email,
            full_name=in_notification.full_name,
            date_updated=in_notification.date_updated,
            field_names=in_notification.field_names,
            notification_sent=False,
            sent_date=None
        ).put()
        return self.http_response(httplib.CREATED)

    def list_customer_modification_notification(self):
        """
        API call will attempt to retrieve all pending customer modification
        notifications.
        """
        pending_notifications = CustomerModificationNotification.query(
            CustomerModificationNotification.notification_sent == False)
        response_json = map(lambda n: {
            'id': n.key.pairs()[0][1],
            'target_email': n.target_email,
            'full_name': n.full_name,
            'date_updated': n.date_updated,
            'field_names': n.field_names
        }, pending_notifications)
        return self.http_response(httplib.NOT_FOUND) if len(response_json) < 1 else \
            self.http_response(httplib.OK, response_json)

    def mark_as_completed(self):
        """
        API call will attempt to mark a set of pending customer modification
        notifications as complete.
        """
        in_notification_ids = InNotificationIds.convert_to_model(self.request.body)
        validation_faults = in_notification_ids.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        notifications = map(lambda _id: CustomerModificationNotification.query(
            CustomerModificationNotification.key == ndb.Key('CustomerModificationNotification', int(_id))
        ).fetch()[0], in_notification_ids.notification_ids)

        def update(n):
            n.notification_sent = True
            n.sent_date = datetime.now()
            n.put()

        map(lambda n: update(n), notifications)
        return self.http_response(httplib.OK)

    post = create_customer_modification_notification
    get = list_customer_modification_notification
    put = mark_as_completed


class CustomerModificationNotificationSender(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer modification
    sender endpoints.
    """

    def send_customer_modification_notifications(self):
        """
        API call is responsible for constructing and sending all currently
        pending customer activation notifications.
        """
        internal_customer_modification_endpoint = BASE_ENDPOINT + 'notification/customer-modification'
        response = self.http_request(
            endpoint=internal_customer_modification_endpoint + "/list/", method='GET')
        if response.status_code == httplib.OK:
            notifications = json_lib.loads(response.content)['success']
            template_directory = \
                os.path.dirname(
                    os.path.realpath(__file__)) + '/templates/customer_modification_template.html'
            with open(template_directory) as template_file:
                template_content = template_file.read()
                api_client = SendGridAPIClient(apikey=SENDGRID_CONFIGURATION['api_key'])
                successful_ids = []
                for n in notifications:
                    template = Template(template_content).render(
                        full_name=n['full_name'],
                        date_updated=n['date_updated'],
                        field_names=n['field_names']
                    )
                    to_email = Email(n['target_email'])
                    from_email = Email(SENDGRID_CONFIGURATION['from_email'])
                    subject = 'Your account has been updated.'
                    content = Content('text/html', template)
                    mail = Mail(from_email, subject, to_email, content)
                    response = api_client.client.mail.send.post(request_body=mail.get())
                    if response.status_code == httplib.ACCEPTED:
                        successful_ids.append(n['id'])
                self.http_request(
                    endpoint=internal_customer_modification_endpoint + '/mark-as-completed/', method='PUT',
                    payload={'notification_ids': successful_ids})

    get = send_customer_modification_notifications


class CustomerActivationNotificationHandler(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer activation
    endpoints to external entities.
    """

    def create_customer_activation_notification(self):
        """
        API call will attempt to create a new customer activation notification
        request. Once created, the notification will be picked up and sent
        by the appropriate CRON job.
        """
        in_notification = InCustomerActivationNotification.convert_to_model(self.request.body)
        validation_faults = in_notification.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        CustomerActivationNotification(
            target_email=in_notification.target_email,
            message=in_notification.message,
            notification_sent=False,
            sent_date=None
        ).put()
        return self.http_response(httplib.CREATED)

    def list_customer_activation_notifications(self):
        """
        API call will attempt to retrieve all pending customer activation
        notifications.
        """
        pending_notifications = CustomerActivationNotification.query(
            CustomerActivationNotification.notification_sent == False)
        response_json = map(lambda n: {
            'id': n.key.pairs()[0][1],
            'target_email': n.target_email,
            'message': n.message
        }, pending_notifications)
        return self.http_response(httplib.NOT_FOUND) if len(response_json) < 1 else \
            self.http_response(httplib.OK, response_json)

    def mark_as_completed(self):
        """
        API call will attempt to mark a set of pending customer activation
        notifications as complete.
        """
        in_notification_ids = InNotificationIds.convert_to_model(self.request.body)
        validation_faults = in_notification_ids.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        notifications = map(lambda _id: CustomerActivationNotification.query(
            CustomerActivationNotification.key == ndb.Key('CustomerActivationNotification', int(_id))
        ).fetch()[0], in_notification_ids.notification_ids)

        def update(n):
            n.notification_sent = True
            n.sent_date = datetime.now()
            n.put()

        map(lambda n: update(n), notifications)
        return self.http_response(httplib.OK)

    post = create_customer_activation_notification
    get = list_customer_activation_notifications
    put = mark_as_completed


class CustomerActivationNotificationSender(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer activation
    sender endpoints.
    """

    def send_customer_activation_notifications(self):
        """
        API call is responsible for constructing and sending all currently
        pending customer activation notifications.
        """
        internal_customer_activation_endpoint = BASE_ENDPOINT + 'notification/customer-activation'
        response = self.http_request(
            endpoint=internal_customer_activation_endpoint + "/list/", method='GET')
        if response.status_code == httplib.OK:
            notifications = json_lib.loads(response.content)['success']
            template_directory = \
                os.path.dirname(os.path.realpath(__file__)) + '/templates/customer_activation_template.html'
            with open(template_directory) as template_file:
                template_content = template_file.read()
                api_client = SendGridAPIClient(apikey=SENDGRID_CONFIGURATION['api_key'])
                successful_ids = []
                for n in notifications:
                    template = Template(template_content).render(activation_link=n['message'])
                    to_email = Email(n['target_email'])
                    from_email = Email(SENDGRID_CONFIGURATION['from_email'])
                    subject = 'Activate your account!'
                    content = Content('text/html', template)
                    mail = Mail(from_email, subject, to_email, content)
                    response = api_client.client.mail.send.post(request_body=mail.get())
                    if response.status_code == httplib.ACCEPTED:
                        successful_ids.append(n['id'])
            self.http_request(
                endpoint=internal_customer_activation_endpoint + '/mark-as-completed/', method='PUT',
                payload={'notification_ids': successful_ids})

    get = send_customer_activation_notifications


class ManagerCreationNotificationHandler(AbstractRestHandler):
    """
    Handler class is responsible for exposing manager creation
    notification endpoints.
    """

    def create_manager_creation_notification(self):
        """
        API call will attempt to create a new customer activation notification
        request. Once created, the notification will be picked up and sent
        by the appropriate CRON job.
        """
        in_notification = InManagerCreationNotification.convert_to_model(self.request.body)
        validation_faults = in_notification.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        ManagerCreationNotification(
            target_email=in_notification.target_email,
            password=in_notification.password,
            notification_sent=False,
            sent_date=None
        ).put()
        return self.http_response(httplib.CREATED)

    def list_manager_creation_notifications(self):
        """
        API call will attempt to retrieve all pending manager creation
        notifications.
        """
        pending_notifications = ManagerCreationNotification.query(
            ManagerCreationNotification.notification_sent == False)
        response_json = map(lambda n: {
            'id': n.key.pairs()[0][1],
            'target_email': n.target_email,
            'password': n.password
        }, pending_notifications)
        return self.http_response(httplib.NOT_FOUND) if len(response_json) < 1 else \
            self.http_response(httplib.OK, response_json)

    def mark_as_completed(self):
        """
        API call will attempt to mark a set of pending manager creation
        notifications as complete.
        """
        in_notification_ids = InNotificationIds.convert_to_model(self.request.body)
        validation_faults = in_notification_ids.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        notifications = map(lambda _id: ManagerCreationNotification.query(
            ManagerCreationNotification.key == ndb.Key('ManagerCreationNotification', int(_id))
        ).fetch()[0], in_notification_ids.notification_ids)

        def update(n):
            n.notification_sent = True
            n.sent_date = datetime.now()
            n.put()

        map(lambda n: update(n), notifications)
        return self.http_response(httplib.OK)

    post = create_manager_creation_notification
    get = list_manager_creation_notifications
    put = mark_as_completed


class ManagerCreationNotificationSender(AbstractRestHandler):
    """
    Handler class is responsible for exposing manager creation
    sender endpoints.
    """

    def send_manager_creation_notifications(self):
        """
        API call is responsible for constructing and sending all currently
        pending manager creation notifications.
        """
        internal_manager_creation_endpoint = BASE_ENDPOINT + 'notification/manager'
        response = self.http_request(
            endpoint=internal_manager_creation_endpoint + "/list/", method='GET')
        if response.status_code == httplib.OK:
            notifications = json_lib.loads(response.content)['success']
            template_directory = \
                os.path.dirname(os.path.realpath(__file__)) + '/templates/manager_creation_template.html'
            with open(template_directory) as template_file:
                template_content = template_file.read()
                api_client = SendGridAPIClient(apikey=SENDGRID_CONFIGURATION['api_key'])
                successful_ids = []
                for n in notifications:
                    template = Template(template_content).render(
                        username=n['target_email'],
                        password=n['password']
                    )
                    to_email = Email(n['target_email'])
                    from_email = Email(SENDGRID_CONFIGURATION['from_email'])
                    subject = 'Your account details'
                    content = Content('text/html', template)
                    mail = Mail(from_email, subject, to_email, content)
                    response = api_client.client.mail.send.post(request_body=mail.get())
                    if response.status_code == httplib.ACCEPTED:
                        successful_ids.append(n['id'])
            self.http_request(
                endpoint=internal_manager_creation_endpoint + '/mark-as-completed/', method='PUT',
                payload={'notification_ids': successful_ids})

    get = send_manager_creation_notifications


class CustomerOrderCreationNotificationHandler(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer order
    creation notification endpoints.
    """

    def create_order_creation_notification(self):
        """
        API call will attempt to create a new customer order creation notification
        request. Once created, the notification will be picked up and sent
        by the appropriate CRON job.
        """
        in_notification = InCustomerOrderCreationNotification.convert_to_model(self.request.body)
        validation_faults = in_notification.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        CustomerOrderCreationNotification(
            target_email=in_notification.target_email,
            order_number=in_notification.order_number,
            products=map(lambda t: CustomerOrderCreationNotification.Product(
                title=t['first'],
                price=t['second']
            ), in_notification.products),
            contact_number=in_notification.contact_number,
            street=in_notification.street,
            city=in_notification.city,
            county=in_notification.county,
            postcode=in_notification.postcode,
            notification_sent=False,
            sent_date=None
        ).put()
        return self.http_response(httplib.CREATED)

    def list_order_creation_notification(self):
        """
        API call will attempt to retrieve all pending customer order
        creation notifications.
        :return:
        """
        pending_notifications = CustomerOrderCreationNotification.query(
            CustomerOrderCreationNotification.notification_sent == False)
        response_json = map(lambda n: {
            'id': n.key.pairs()[0][1],
            'target_email': n.target_email,
            'order_number': n.order_number,
            'products': map(lambda product: {
                'title': product.title,
                'price': product.price
            }, n.products),
            'contact_number': n.contact_number,
            'street': n.street,
            'city': n.city,
            'county': n.county,
            'postcode': n.postcode
        }, pending_notifications)
        return self.http_response(httplib.NOT_FOUND) if len(response_json) < 1 else \
            self.http_response(httplib.OK, response_json)

    def mark_as_completed(self):
        """
        API call will attempt to mark a set of pending customer order
        creation notifications as complete.
        """
        in_notification_ids = InNotificationIds.convert_to_model(self.request.body)
        validation_faults = in_notification_ids.validate()
        if validation_faults:
            logging.error(validation_faults)
            return self.http_response(httplib.BAD_REQUEST, validation_faults)
        notifications = map(lambda _id: CustomerOrderCreationNotification.query(
            CustomerOrderCreationNotification.key == ndb.Key('CustomerOrderCreationNotification', int(_id))
        ).fetch()[0], in_notification_ids.notification_ids)

        def update(n):
            n.notification_sent = True
            n.sent_date = datetime.now()
            n.put()

        map(lambda n: update(n), notifications)
        return self.http_response(httplib.OK)

    post = create_order_creation_notification
    get = list_order_creation_notification
    put = mark_as_completed


class CustomerOrderCreationNotificationSender(AbstractRestHandler):
    """
    Handler class is responsible for exposing customer order creation
    sender endpoints.
    """

    def send_create_order_notification(self):
        """
        API call is responsible for constructing ans sending all currently
        pending order creation notifications.
        """
        internal_order_creation_notification_endpoint = BASE_ENDPOINT + 'notification/order-creation'
        response = self.http_request(
            endpoint=internal_order_creation_notification_endpoint + "/list/", method='GET')
        if response.status_code == httplib.OK:
            notifications = json_lib.loads(response.content)['success']
            template_directory = \
                os.path.dirname(os.path.realpath(__file__)) + '/templates/customer_order_creation_template.html'
            with open(template_directory) as template_file:
                template_content = template_file.read()
                api_client = SendGridAPIClient(apikey=SENDGRID_CONFIGURATION['api_key'])
                successful_ids = []
                for n in notifications:
                    template = Template(template_content).render(
                        username=n['target_email'],
                        target_email=n['target_email'],
                        order_number=n['order_number'],
                        products=n['products'],
                        product_total="{0:.2f}".format(round(sum(map(lambda t: float(t['price']), n['products'])), 2)),
                        contact_number=n['contact_number'],
                        street=n['street'],
                        city=n['city'],
                        county=n['county'],
                        postcode=n['postcode']
                    )
                    to_email = Email(n['target_email'])
                    from_email = Email(SENDGRID_CONFIGURATION['from_email'])
                    subject = 'Thank you for your order'
                    content = Content('text/html', template)
                    mail = Mail(from_email, subject, to_email, content)
                    response = api_client.client.mail.send.post(request_body=mail.get())
                    if response.status_code == httplib.ACCEPTED:
                        successful_ids.append(n['id'])
            self.http_request(
                endpoint=internal_order_creation_notification_endpoint + '/mark-as-completed/', method='PUT',
                payload={'notification_ids': successful_ids})

    get = send_create_order_notification

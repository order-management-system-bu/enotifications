#!/usr/bin/env bash
# Simple shell script is responsible for building
# the notifications service
# Create an external library directory for Google App Engine
sudo rm -r lib/
mkdir lib/
# Install service dependencies
pip2 install -t lib/ ../utilities/dist/enotifications_utility_module-0.1.tar.gz
pip2 install -t lib/ -r requirements.txt
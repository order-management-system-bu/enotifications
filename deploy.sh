#!/usr/bin/env bash
gcloud config set project e-notifications-230119
gcloud app deploy --version 1 \
    cron.yaml \
    dispatch.yaml \
    enotifications/app.yaml

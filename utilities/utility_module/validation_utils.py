from abstractmodel import AbstractModel


def verify_mandatory_fields(model):
    """
    Function will ensure that all designated mandatory fields are
    accounted for.
    :param model:  Instance of AbstractModel to be verified.
    :raises TypeError if the given model is not an AbstractModel.
    :return: Optional list of validation faults.
    """
    if not isinstance(model, AbstractModel):
        raise TypeError('Expected an AbstractMode but a ' + str(type(model)) + ' was provided.')
    model_as_dict = model.__dict__
    validation_faults = []
    for mf in model.mandatory_fields:
        # Pythons truthy won't work here as False is a valid value.
        if model_as_dict[mf] != False and not model_as_dict[mf]:
            validation_faults.append('The mandatory ' + _beautify_field_name(mf) +
                                     ' field was expected but not provided.')
        # Need to validated any nested AbstractModel instances.
        elif isinstance(model_as_dict[mf], AbstractModel):
            validation_faults += verify_mandatory_fields(model_as_dict[mf])
    return validation_faults


def verify_max_length(model, fields, max_length):
    """
    Function will verify that the provided fields do not exceed the
    given maximum length.
    :param model: Instance of AbstractModel to be verified.
    :param fields: List of field names to be verified.
    :param max_length: Maximum length of a field before it becomes invalid.
    :raises TypeError if the given model is not an AbstractModel.
    :returns Optional list of validation faults.
    """
    if not isinstance(model, AbstractModel):
        raise TypeError('Expected an AbstractMode but a ' + str(type(model)) + ' was provided.')
    model_as_dict = model.__dict__
    faulty_fields = filter(lambda fn: model_as_dict[fn] and len(model_as_dict[fn]) > max_length, fields)
    validation_faults = map(lambda fn: 'The provided field: ' + _beautify_field_name(fn) +
                                       ' exceeded the maximum length of ' + str(max_length), faulty_fields)
    return validation_faults


def _beautify_field_name(field_name):
    """
    Function is responsible for beautifying the provided field name
    so that it may be used as part of an output String. Solution
    was taken from: https://stackoverflow.com/questions/1549641/how-to-capitalize-the-first-letter-of-each-word-in-a-string-python
    """
    remove_underscores = field_name.replace('_', ' ')
    capitalise_name = ' '.join(s[:1].upper() + s[1:] for s in remove_underscores.split(' '))
    return capitalise_name

import json as json_lib

import webapp2
from google.appengine.api import urlfetch


class AbstractRestHandler(webapp2.RequestHandler):
    """
    Abstract class is responsible for providing the necessary
    request and response functionality for all handlers in
    the OMS-ENotifications application.
    """

    def http_response(self, status, payload=None):
        """
        Method will construct a server response with an optional
        JSON payload.
        :param status: HTTP status of the response.
        :param payload: Optional JSON to be included in the response.
        """
        self.response.status_int = status
        if payload:
            self.response.headers['Content-Type'] = 'application/json; charset=utf-8'
            formatted_payload = {'error': payload} if str(status)[0] != '2' else {'success': payload}
            self.response.write(json_lib.dumps(formatted_payload))

    @classmethod
    def http_request(cls, endpoint, method, headers=None, payload=None):
        """
        Method will construct and send a HTTP request the the provided endpoint.
        :param endpoint: Target endpoint for the request.
        :param method: HTTP method to be used in the request.
        :param headers: Optional headers to be included in the request.
        :param payload: Optional JSON payload to be included in the request.
        :raises TypeError if any additional headers are not of type Dict
        :raises ValueError if the provided HTTP method is unrecognised.
        """
        additional_headers = {}
        if headers:
            if isinstance(headers, list):
                if len(filter(lambda h: not isinstance(h, dict), headers)) > 0:
                    raise TypeError('One of more provided headers were not dicts.')
                additional_headers += headers
            elif isinstance(headers, dict):
                additional_headers.update(headers)
            else:
                raise TypeError('Provided headers must be dicts.')
        if method.upper() == 'GET':
            return urlfetch.fetch(url=endpoint, headers=additional_headers)
        elif method.upper() == 'POST':
            additional_headers.update({'Content-Type': 'application/json'})
            return urlfetch.fetch(url=endpoint, payload=json_lib.dumps(payload), method=urlfetch.POST,
                                  headers=additional_headers)
        elif method.upper() == 'PUT':
            additional_headers.update({'Content-Type': 'application/json'})
            return urlfetch.fetch(url=endpoint, payload=json_lib.dumps(payload), method=urlfetch.PUT,
                                  headers=additional_headers)
        else:
            raise ValueError('An invalid method was provided.')

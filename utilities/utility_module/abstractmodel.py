import json as json_lib
from abc import ABCMeta, abstractproperty, abstractmethod


class AbstractModel:
    """
    Abstract class is responsible for providing the necessary
    validation/JSON conversion logic for all models in the
    OMS-ENotifications application
    """
    __metaclass__ = ABCMeta

    @abstractproperty
    def mandatory_fields(self):
        """
        List of field names that are to be considered mandatory
        for this model.
        """

    @abstractmethod
    def validate(self):
        """
        Method is responsible for validating an instance of this
        against a provided set of conditions.
        :return: List of validation fault Strings.
        """

    @classmethod
    @abstractmethod
    def _read_json(cls, json):
        """
        Method must take a JSON dict and return an instance of this.
        :return: New instance of this model.
        """

    @classmethod
    def convert_to_model(cls, json):
        """
        Method is responsible for converting the provided JSON into
        an instance of this model.
        :param json: JSON to be converted into a model.
        :return: New instance of this model.
        """
        if not json:
            return None
        if isinstance(json, str):
            json = json_lib.loads(json)
        return cls._read_json(json)

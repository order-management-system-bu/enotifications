#!/usr/bin/env bash
# Simple shell script is responsible for building and
# installing the ENotifications Utility Module to the
# projects virtual environment.
python2 setup.py sdist
pip2 install dist/enotifications_utility_module-0.1.tar.gz
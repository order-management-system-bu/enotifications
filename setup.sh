#!/usr/bin/env bash
# Simple shell script is responsible for setting up the
# OMS-ENotifications application.
# Create a new python2.7 virtual environment.
virtualenv -p `which python2.7` venv
source venv/bin/activate
# Build utility module
cd utilities
./install.sh
# Build services
cd ../enotifications
./build.sh
cd ../
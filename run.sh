#!/usr/bin/env bash
# Simple shell script will run the application on
# Google App Engines development server.
python2 /opt/google-cloud-sdk/bin/dev_appserver.py \
    dispatch.yaml \
    enotifications/app.yaml